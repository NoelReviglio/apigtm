package ar.com.tallermecanico.web.logica;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.tallermecanico.web.modelo.Entorno;
import ar.com.tallermecanico.web.modelo.Vehiculo;

import ar.com.tallermecanico.web.repositorio.VehiculoRepositorio;

import ar.com.tallermecanico.web.util.Mensaje;
import ar.com.tallermecanico.web.util.Respuesta;
import ar.com.tallermecanico.web.util.Validaciones;

@Service
public class VehiculoServicio {

    @Autowired
    private VehiculoRepositorio repositorio;
    @Autowired
    private OrdenDeServicioServicio servicioOS;

    //ALTA
    public Respuesta registrarVehiculo(Vehiculo v, Entorno entorno) {
        Respuesta rta = null;
        try{
            Validaciones validaciones = new Validaciones();
            
            v.setEntorno(entorno);
            if (repositorio.findByDominioIgnoreCaseAndEntorno(v.getDominio().toUpperCase(), v.getEntorno()) != null) {
                validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_01: El dominio que intenta registrar: " + v.getDominio().toUpperCase() + " ya existe"));
            }

            if(validaciones.isValid()) {
                //Convertir a mayúsculas Dominio, Chasis, Modelo y Color
                v.setDominio(v.getDominio().toUpperCase());
                v.setModelo(v.getModelo().toUpperCase());
                v.setColor(v.getColor().toUpperCase());
                v.setChasis(v.getChasis().toUpperCase());
                //Validar Propietario y convertir a mayúsculas
                if (v.getRazonSocialPropietario() == null) {
                    v.setApellidoPropietario(v.getApellidoPropietario().toUpperCase());
                    v.setNombrePropietario(v.getNombrePropietario().toUpperCase());
                } else {
                    v.setRazonSocialPropietario(v.getRazonSocialPropietario().toUpperCase());
                }

                repositorio.save(v); 
		        rta = new Respuesta("OK", "Guardado con Éxito", (Object) v);
	    	}else {
	    		rta = new Respuesta("ERRORES: ", validaciones.getMensajes(), (Object) v);
	    	}
        } 
        catch(Exception e) {
    		e.printStackTrace();
    		rta = new Respuesta("EXCEPCION_ERROR: ", e.toString(), (Object) v);
        }
        return rta;
    }

    //BAJA
    public Respuesta eliminarVehiculo(Long id) {
        Respuesta rta = null;
        try{
            Validaciones validaciones = new Validaciones();
            Vehiculo v = repositorio.getOne(id);
            if (v.getId() == null) {
                validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_01: No se indicó ningún ID asociado o el mismo no corresponde a un Vehículo registrado"));
            };
            
            if (!servicioOS.obtenerOSporVehiculo(v).isEmpty()) {
                validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_02: El vehículo indicado no se puede eliminar porque posee asociadas 1 o más Órdenes de Servicio"));
            }

            if(validaciones.isValid()) {
                repositorio.deleteById(id);
                rta = new Respuesta("OK", "El vehículo se ha ELIMINADO correctamente");
	    	}else {
	    		rta = new Respuesta("ERRORES: ", validaciones.getMensajes());
	    	}
        } 
        catch(Exception e) {
    		e.printStackTrace();
    		rta = new Respuesta("EXCEPCION_ERROR: ", e.toString());
        }

        return rta;

    }

    //MODIFICACION
    public Respuesta modificarVehiculo(Vehiculo v, Entorno entorno) {
        Respuesta rta = null;
        try{
            Validaciones validaciones = new Validaciones();
            v.setEntorno(entorno);
            // Valido que no mande un objeto null
            if (v.getId() != null) {

                // Busco en BD por dominio para validar que el dominio que quiero cargar no exista
                Vehiculo vGuardado = repositorio.findByDominioIgnoreCaseAndEntorno(v.getDominio().toUpperCase(), v.getEntorno()); 
                
                // Valido que si se encontró un objeto, y los id son distintos, ya existe ese dominio. Si es null es porque no existe el dominio y puede seguir.
                if ((vGuardado != null) && (v.getId() != vGuardado.getId())) {
                    validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_02: El dominio que intenta registrar: " + v.getDominio().toUpperCase() + " ya existe"));
                } else {
                    // Busco en BD por el ID, para trabajar sobre el mismo objeto
                    vGuardado = repositorio.getOne(v.getId());
                    
                   /*  //Valida que no tenga OS en estado Abierta.
                    if (servicioOS.modificable(vGuardado)) {
                        validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_04: El vehículo indicado no se puede modificar porque posee asociadas Órdenes de Servicio en estado: ABIERTA. Cierre estas órdenes primero e intente nuevamente."));
                    } */
                    
                    // Valido si los dominios son iguales: si modificó el dominio, debo validar que no posea OS registradas. Si modificó otro campo, debo validar que no tenga OS en estado Abierta.
                    if (v.getDominio().equalsIgnoreCase(vGuardado.getDominio())) {
                        //Dominios iguales, por tanto se modificó otro campo, valida que no tenga OS en estado Abierta.
                        
                        //El siguiente IF valida si se modificaron campos propietarios. Si son otros los campos, no hay problema. El dominio se validó previamente.
                        if (( !((""+v.getApellidoPropietario()+v.getNombrePropietario()+v.getRazonSocialPropietario()).toUpperCase()).equals((""+vGuardado.getApellidoPropietario()+vGuardado.getNombrePropietario()+vGuardado.getRazonSocialPropietario()).toUpperCase())) && (servicioOS.modificable(vGuardado))) {
                            validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_04: No se pueden modificar los datos del Propietario del vehículo indicado porque posee asociadas Órdenes de Servicio en estado: ABIERTA. Cierre estas órdenes primero e intente nuevamente."));
                        
                        //El siguiente IF valida todos los campos del vehiculo restantes. No diferencia propietario. El dominio se validó previamente.
                        /* if (servicioOS.modificable(vGuardado)) {    
                            validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_04: No se pueden modificar los datos del vehículo indicado porque posee asociadas Órdenes de Servicio en estado: ABIERTA. Cierre estas órdenes primero e intente nuevamente.")); */
                        
                        }
                    } else {
                        //Dominios distintos, sólo modifica si no posee OS registradas
                        if (!servicioOS.obtenerOSporVehiculo(vGuardado).isEmpty()) {
                            validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_03: No puede cambiar el dominio original: " + vGuardado.getDominio().toUpperCase() + " por " + v.getDominio().toUpperCase() + " porque el primero posee asociadas 1 o más Órdenes de Servicio"));
                        }
                    }

                }
            } else {
                validaciones.mensajes.add(new Mensaje("ERROR: ", "ERROR_01: No se indicó ningún ID asociado o el mismo no corresponde a un Vehículo registrado"));     
            }

            if(validaciones.isValid()) {
                //Convertir a mayúsculas Dominio, Modelo y Color
                v.setDominio(v.getDominio().toUpperCase());
                v.setModelo(v.getModelo().toUpperCase());
                v.setColor(v.getColor().toUpperCase());
                //Validar Propietario y convertir a mayúsculas
                if (v.getRazonSocialPropietario() == null) {
                    v.setApellidoPropietario(v.getApellidoPropietario().toUpperCase());
                    v.setNombrePropietario(v.getNombrePropietario().toUpperCase());
                } else {
                    v.setRazonSocialPropietario(v.getRazonSocialPropietario().toUpperCase());
                }
                
                repositorio.save(v); 
		        rta = new Respuesta("OK", "Modificación guardada con Éxito", (Object) v);
	    	}else {
	    		rta = new Respuesta("ERRORES: ", validaciones.getMensajes(), (Object) v);
	    	}
        } 
        catch(Exception e) {
    		e.printStackTrace();
    		rta = new Respuesta("EXCEPCION_ERROR: ", e.toString(), (Object) v);
        }

        return rta;
    }
         

    //CONSULTA
    public Page<Vehiculo> consultarVehiculo(String dominio, Entorno entorno, Pageable pagina) {
        return repositorio.findByDominioIgnoreCaseAndEntorno(dominio, entorno, pagina);
    }

    public Page<Vehiculo> getAllVehiculo(Entorno entorno, Pageable pagina) {
        return repositorio.findByEntornoOrderByDominio(entorno, pagina);
    }

    public Page<Vehiculo> getAllVehiculoByDominio(String dominio, Entorno entorno, Pageable pagina) {
		return repositorio.findByDominioContainingIgnoreCaseAndEntornoOrderByDominio(dominio, entorno, pagina);
	}
    
    public List<Vehiculo> getAll(Entorno entorno) {
    	return repositorio.findByEntorno(entorno);
    }

    //OTROS MÉTODOS

}
