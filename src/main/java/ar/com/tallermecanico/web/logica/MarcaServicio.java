package ar.com.tallermecanico.web.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.tallermecanico.web.modelo.Marca;
import ar.com.tallermecanico.web.repositorio.MarcaRepositorio;


@Service
public class MarcaServicio {

    @Autowired
    private MarcaRepositorio repositorio;

    //CONSULTA
    
    public List<Marca> getAllMarca() {
        return repositorio.findAll();
    }
}