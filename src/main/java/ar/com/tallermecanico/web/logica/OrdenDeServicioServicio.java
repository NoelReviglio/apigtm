package ar.com.tallermecanico.web.logica;

import java.nio.charset.CodingErrorAction;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ar.com.tallermecanico.web.modelo.DetalleOrdenDeServicio;
import ar.com.tallermecanico.web.modelo.Entorno;
import ar.com.tallermecanico.web.modelo.Estado;
import ar.com.tallermecanico.web.modelo.OrdenDeServicio;
import ar.com.tallermecanico.web.modelo.Vehiculo;
import ar.com.tallermecanico.web.repositorio.DetalleServicioRepositorio;
import ar.com.tallermecanico.web.repositorio.EstadoRepositorio;
import ar.com.tallermecanico.web.repositorio.OrdenDeServicioRepositorio;
import ar.com.tallermecanico.web.repositorio.VehiculoRepositorio;
import ar.com.tallermecanico.web.util.Mensaje;
import ar.com.tallermecanico.web.util.Respuesta;
import lombok.NonNull;

@Service
public class OrdenDeServicioServicio {
    
    @Autowired
	private OrdenDeServicioRepositorio repositorio;
	@Autowired
    private VehiculoRepositorio repositorioV;
    @Autowired
    private VehiculoServicio _servicioVehiculo;
    @Autowired
    private DetalleServicioRepositorio _repositorioDetalle;
    @Autowired
    private EstadoRepositorio _repositorioEstado;
    private ValidacionesSistema validaciones;
    
    //ALTA
	private OrdenDeServicio guardarOrdenDeServicio(OrdenDeServicio os) {
		//Convertir a mayúsculas Chofer
		os.setApellidoChofer(os.getApellidoChofer().toUpperCase());
		os.setNombreChofer(os.getNombreChofer().toUpperCase());
 
		return repositorio.save(os);
	}
	
    public Respuesta registrarOrdenDeServicio(OrdenDeServicio os, Entorno entorno) {
    	Respuesta rta = null;
    
    	try {
    		validaciones = new ValidacionesSistema();
	    	os = seteoFechas(os);
	    	if(os.isAbonada()) {
	    		os.setEstado(seteoEstado("Abonada"));
	    	}else {
	    		os.setEstado(seteoEstado("Abierta"));
	    	}
	    	os.setEntorno(entorno);
		    os.setVehiculo((Vehiculo) guardarVehiculo(os.getVehiculo()).getObjeto());
		    os.setDetalle(registrarDetalleServicio(os.getDetalle()));
		    if(validaciones.isValid()) {
		    	os.setCodigo(obtenerUltimoCodigo(os.getEntorno().getId()));
		    	guardarOrdenDeServicio(os);
		        rta = new Respuesta("OK", "Guardado con Éxito.", (Object) os);
	    	}else {
	    		rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
	    	}     
    	}catch(Exception e) {
    		e.printStackTrace();
    		rta = new Respuesta("EXCEPCION_ERROR: ", "Error al guardar la Orden de Servicio."+e.toString(), (Object) os);
    	}
    	
        return rta;
    }

	public List<DetalleOrdenDeServicio> registrarDetalleServicio(List<DetalleOrdenDeServicio> detalle) {
		if(detalle.isEmpty()) {
			validaciones.mensajes.add(new Mensaje("ERROR","ERROR2: No se encontraron Detalles de Servicios en la Orden asociada"));
		}
		List<DetalleOrdenDeServicio> lista = new ArrayList<DetalleOrdenDeServicio>();
		for (DetalleOrdenDeServicio d : detalle) {
			if(d.getId()==0) {
				DetalleOrdenDeServicio det = new DetalleOrdenDeServicio(d.getDescripcion(),d.getSubtotal());
				lista.add(det);
			} else {
				lista.add(d);
			}	
		}
		if(validaciones.isValid()) {
			return _repositorioDetalle.saveAll(lista);
		} else {
			return lista;
		}
	}

	//BAJA LOGICA
	public Respuesta anularOrdenDeServicio(Long codigo, Entorno entorno) {
		Respuesta rta = null;
		OrdenDeServicio os = null;
    	try {
	    	validaciones = new ValidacionesSistema();
			os = repositorio.findByCodigoAndEntorno(codigo, entorno);
			if(estadoValido(os)){
				os.setEstado(seteoEstado("Anulada"));
				os = guardarOrdenDeServicio(os);
				rta = new Respuesta("OK", "Guardado con Éxito.", (Object) os);
	    	}else {
	    		validaciones.mensajes.add(new Mensaje("ERROR","ERROR3:No se puede Anular la Orden de Servicio, dado que ya cuenta con el estado "+os.getEstado().getNombre()+"."));
	    		rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
			}
    	}catch(Exception e) {
    		e.printStackTrace();
    		rta = new Respuesta("ERROR", "Error al Anular la Orden de Servicio.", (Object) os);
    	}
        return rta;
	}

    //MODIFICACION
    public Respuesta modificarOrdenDeServicio(OrdenDeServicio os, Entorno entorno) {
    	Respuesta rta = null;
        
	    try {
	    	validaciones = new ValidacionesSistema();
	    	if (os.getCodigo() == null) {
	    		validaciones.mensajes.add(new Mensaje("ERROR","ERROR4: No se indicó ningún Código de Orden de Servicio asociado"));
	        }
	        if(estadoValido(os)) {
	        	os = seteoFechas(os);
	        	os.setEntorno(entorno);
	        	os.setDetalle(registrarDetalleServicio(os.getDetalle()));
	        	if(os.isAbonada()) {
	        		os.setEstado(seteoEstado("Abonada"));
	        	}
	        	if(validaciones.isValid()) {
		        	guardarOrdenDeServicio(os);
		        	rta = new Respuesta("OK", "Guardado con Éxito.", (Object) os);
	    		}else {
	    			rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
	    		}
	        } else {
	        	validaciones.mensajes.add(new Mensaje("ERROR","ERROR5: No se puede modificar la Orden de Servicio, dado que ya cuenta con el estado final "+os.getEstado().getNombre()+"."));
	        	rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
	        }
	    }catch(Exception e) {
    		e.printStackTrace();
    		rta = new Respuesta("ERROR", "Error al Modificar la Orden de Servicio.", (Object) os);
    	}
        return rta;
    }

	public Respuesta guardarVehiculo(Vehiculo vehiculo) {
		return _servicioVehiculo.modificarVehiculo(vehiculo, vehiculo.getEntorno());
	}

	//CONSULTA
    public Page<OrdenDeServicio> consultarOrdenDeServicio(Long codigo, Entorno entorno, Pageable pagina) {
        return repositorio.findByCodigoAndEntorno(codigo, entorno, pagina);
    }
    
    public OrdenDeServicio consultarOrdenDeServicio(Long codigo, Entorno entorno) {
        return repositorio.findByCodigoAndEntorno(codigo, entorno);
    }
    
    public Page<OrdenDeServicio> getAllOrdenDeServicio(Entorno entorno, Pageable pagina) {
        return repositorio.findByEntornoOrderByCodigoDesc(entorno, pagina);
	}
	
	//Usado en la validación de Eliminar vehículo
	public List<OrdenDeServicio> obtenerOSporVehiculo(Vehiculo v) {
		return repositorio.findByVehiculo(v);
	}

	//Usado para traer OS por Vehículo
	public Page<OrdenDeServicio> obtenerOSporVehiculo(String dominio, Entorno entorno, Pageable pagina) {
		Vehiculo v = repositorioV.findByDominioIgnoreCaseAndEntorno(dominio, entorno);
        return repositorio.findByVehiculoOrderByCodigoDesc(v, pagina);
    }

    //OTROS MÉTODOS

	public OrdenDeServicio seteoFechas(OrdenDeServicio os) {
		String fec = os.getFecha().toString();
		LocalDate fecha = LocalDate.parse(fec);
		os.setFecha(fecha);
		if (! (os.getFechaRecepcion() == null)) {
			String fecRecepcion = os.getFechaRecepcion().toString();
			LocalDate fechaRecepcion = LocalDate.parse(fecRecepcion);
			os.setFechaRecepcion(fechaRecepcion);
		}
		return os;
	}
	
	public Estado seteoEstado(String nombre) {
    	return _repositorioEstado.findByNombreIgnoreCase(nombre);

	}

    private Long obtenerUltimoCodigo(Integer entornoId) {
		Long codigoMaximo = repositorio.getMaxCodigo(entornoId);
		if (codigoMaximo == null) {
			codigoMaximo = (long) 0;
		}
		return codigoMaximo + 1;
	}
    
	public Respuesta abonarOrdenDeServicio(OrdenDeServicio os, Entorno entorno) {
		Respuesta rta = null;
        
	    try {
		    validaciones = new ValidacionesSistema();
		    os.setEntorno(entorno);
			if (validarAbonada(os)) {
				os.setAbonada(true);
				os.setEstado(seteoEstado("Abonada"));
				if(validaciones.isValid()) {
					guardarOrdenDeServicio(os);
					rta = new Respuesta("OK", "Guardado con Éxito.", (Object) os);
    			}else {
    				rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
    			}
        	} else {
        		validaciones.mensajes.add(new Mensaje("ERROR","ERROR6: No se puede modificar la Orden de Servicio, dado que ya cuenta con el estado final "+os.getEstado().getNombre()+"."));
        		rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
        	}
	    }catch(Exception e) {
    		e.printStackTrace();
    		rta = new Respuesta("ERROR", "Error al marcar como Abonada a la Orden de Servicio.", (Object) os);
    	}
        return rta;
	}
	
	public Respuesta cerrarOrdenDeServicio(OrdenDeServicio os, Entorno entorno) {
		Respuesta rta = null;
        
	    try {
		    validaciones = new ValidacionesSistema();
		    os.setEntorno(entorno);
			if (estadoValido(os)) {
				os.setEstado(seteoEstado("Cerrada"));
				if(validaciones.isValid()) {	
					guardarOrdenDeServicio(os);
					rta = new Respuesta("OK", "Guardado con Éxito.", (Object) os);
				}else {
					rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
				}
			} else {
				validaciones.mensajes.add(new Mensaje("ERROR","ERROR6: No se puede modificar la Orden de Servicio, dado que ya cuenta con el estado final "+os.getEstado().getNombre()+"."));
				rta = new Respuesta("ERROR", validaciones.getMensajes(), (Object) os);
			}
	    }catch(Exception e) {
	    	e.printStackTrace();
			rta = new Respuesta("ERROR", "Error al marcar como Cerrada a la Orden de Servicio.", (Object) os);
		}
	    return rta;
	}
	
	//VALIDACIONES
    
	private boolean estadoValido(OrdenDeServicio os) {
    	if((os.getEstado().getNombre().equalsIgnoreCase("Abierta"))) {
    		return true;
    	}	
    	return false;
	}
	
    private boolean validarAbonada(OrdenDeServicio os) {
    	if(!((os.getEstado().getNombre().equalsIgnoreCase("Anulada")) || (os.getEstado().getNombre().equalsIgnoreCase("Abonada")))) {
    		return true;
    	}
    	return false;
	}

    public class ValidacionesSistema extends Exception{
    	private List<Mensaje> mensajes = new ArrayList<Mensaje>();

		public List<Mensaje> getMensajes() {
			return mensajes;
		}

		public void setMensajes(List<Mensaje> mensajes) {
			this.mensajes = mensajes;
		} 
		
		public boolean isValid() {
			return mensajes.size()==0;
		}
    }

    //No valido entorno dado que sólo es llamado desde VehículoServicio y ahí ya se le setea.
	public boolean modificable(Vehiculo vehiculo) {
		List<OrdenDeServicio> lista = obtenerOSporVehiculo(vehiculo);
		boolean bandera = false;
		if(!lista.isEmpty()) {
			for (OrdenDeServicio os : lista) {
				if(estadoValido(os)){
					bandera = true;
				}
			}
		} 
		return bandera;
	}
}