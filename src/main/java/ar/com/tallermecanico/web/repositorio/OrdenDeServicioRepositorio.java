package ar.com.tallermecanico.web.repositorio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ar.com.tallermecanico.web.modelo.Entorno;
import ar.com.tallermecanico.web.modelo.OrdenDeServicio;
import ar.com.tallermecanico.web.modelo.Vehiculo;

@Repository
public interface OrdenDeServicioRepositorio extends JpaRepository<OrdenDeServicio, Long> {

	@Query("Select MAX(codigo) from OrdenDeServicio where entorno_id=?1")
	Long getMaxCodigo(Integer entorno);

	Page<OrdenDeServicio> findByEntornoOrderByCodigoDesc(Entorno entorno, Pageable pagina);
	
	Page<OrdenDeServicio> findByCodigoAndEntorno(Long codigo, Entorno entorno, Pageable pagina);

	OrdenDeServicio findByCodigoAndEntorno(Long codigo, Entorno entorno);

	List<OrdenDeServicio> findByVehiculo(Vehiculo vehiculo);

	Page<OrdenDeServicio> findByVehiculoOrderByCodigoDesc(Vehiculo vehiculo, Pageable pagina);
    
}