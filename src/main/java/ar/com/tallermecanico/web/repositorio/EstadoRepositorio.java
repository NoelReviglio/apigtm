package ar.com.tallermecanico.web.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.tallermecanico.web.modelo.Estado;
import lombok.NonNull;

@Repository
public interface EstadoRepositorio extends JpaRepository<Estado,Integer>  {

	Estado findByNombreIgnoreCase(String nombre);

}
