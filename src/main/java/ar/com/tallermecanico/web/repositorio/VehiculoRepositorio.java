package ar.com.tallermecanico.web.repositorio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.tallermecanico.web.modelo.Entorno;
import ar.com.tallermecanico.web.modelo.Vehiculo;

@Repository
public interface VehiculoRepositorio extends JpaRepository<Vehiculo, Long> {

    Page<Vehiculo> findByDominioContainingIgnoreCaseAndEntornoOrderByDominio(String dominio, Entorno entorno, Pageable pagina);

    Vehiculo findByDominioIgnoreCaseAndEntorno(String dominio, Entorno entorno);

    Page<Vehiculo> findByDominioIgnoreCaseAndEntorno(String dominio, Entorno entorno, Pageable pagina);
    
    List<Vehiculo> findByEntorno(Entorno entorno);
    
    Page<Vehiculo> findByEntornoOrderByDominio(Entorno entorno,Pageable pagina);

}