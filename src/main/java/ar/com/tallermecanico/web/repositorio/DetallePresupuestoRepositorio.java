package ar.com.tallermecanico.web.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.tallermecanico.web.modelo.DetalleOrdenDeServicio;
import ar.com.tallermecanico.web.modelo.DetallePresupuesto;

@Repository
public interface DetallePresupuestoRepositorio extends JpaRepository<DetallePresupuesto, Long>{

}
