package ar.com.tallermecanico.web.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.tallermecanico.web.modelo.Presupuesto;

@Repository
public interface PresupuestoRepositorio extends JpaRepository<Presupuesto,Integer>{

}
