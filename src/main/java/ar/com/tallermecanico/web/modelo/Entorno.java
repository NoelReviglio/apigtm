package ar.com.tallermecanico.web.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Entity
public class Entorno {

	@Id
	@GeneratedValue
	private Integer id;
	@NonNull
	private String nombre;
	//URI de la imagen del cliente
	@NonNull
	private String url;
	//Utilizado en los reportes
	@NonNull
	private String direccion;
	@NonNull
	private String telefono;
	private String nota;
	
}