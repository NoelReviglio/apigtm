package ar.com.tallermecanico.web.modelo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.sun.istack.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Entity
public class Presupuesto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	@ManyToOne @NotNull
	private Entorno entorno;
	@NotNull
	private LocalDate fecha;
	@NotNull
	private String nombreTitular;
	private String apellidoTitular;
	private String razonSocialTitular;
	private Long telefonoTitular;
	@ManyToOne @NonNull
	private Vehiculo vehiculo;
	private String descripcionCliente;
	@OneToMany(fetch=FetchType.EAGER) @NotNull
	private List<DetallePresupuesto> detalle;
	@NonNull @NotNull
	private BigDecimal total;
	@ManyToOne @NotNull
	private Estado estado;
	private Integer diasValidez;
}
