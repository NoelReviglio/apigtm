package ar.com.tallermecanico.web.modelo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.sun.istack.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Entity
public class OrdenDeServicio{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	@ManyToOne @NotNull
	private Entorno entorno;
	@NotNull
	private LocalDate fecha;
	private String nombreTitular;
	private String apellidoTitular;
	private String razonSocialTitular;
	private Long telefonoTitular;
	private String nombreChofer;
	private String apellidoChofer;
	private Long telefonoChofer;
	@ManyToOne @NonNull
	private Vehiculo vehiculo;
	private BigDecimal nivelCombustible;
	@NotNull
	private Integer kilometrajeActual;
	private LocalDate fechaRecepcion;
	private LocalTime horaRecepcion;
	@NotNull
	private String motivoVisita;
	@OneToMany(fetch=FetchType.EAGER) @NotNull
	private List<DetalleOrdenDeServicio> detalle;
	@NonNull @NotNull
	private BigDecimal total;
	private boolean abonada;
	@ManyToOne @NotNull
	private Estado estado;
	
}
