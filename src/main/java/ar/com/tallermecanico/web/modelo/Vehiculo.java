package ar.com.tallermecanico.web.modelo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.sun.istack.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Vehiculo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne @NotNull
	private Entorno entorno; 
	@NotNull
	private String dominio;
	@ManyToOne @NotNull
	private Marca marca;
	@NotNull
	private String modelo;
	private String chasis;
	@NotNull
	private Integer anioModelo;
	@NotNull
	private String color;
	@NotNull
	private Integer kilometraje;
	private String nombrePropietario;
	private String apellidoPropietario;
	private String razonSocialPropietario;
	private Long telefonoPropietario;
}
