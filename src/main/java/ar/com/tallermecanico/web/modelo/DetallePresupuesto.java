package ar.com.tallermecanico.web.modelo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sun.istack.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class DetallePresupuesto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String descripcion;
	@NotNull
	private BigDecimal subtotal;
	
	public DetallePresupuesto(String descripcion, BigDecimal subtotal) {
		setDescripcion(descripcion);
		setSubtotal(subtotal);
	}
}
