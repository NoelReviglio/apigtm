package ar.com.tallermecanico.web.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;

import ar.com.tallermecanico.web.logica.VehiculoServicio;
import ar.com.tallermecanico.web.modelo.Vehiculo;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ReporteXls extends TemplateXlsGeneric {
	
	VehiculoServicio servicio;

	public ReporteXls(VehiculoServicio servicio) {
		this.servicio = servicio;
	}

	@Override
	public String getNameSheet() {
		return "Notas alumnos";
	}

	@Override
	public String getTitle() {
		return "Notas alumnos";
	}

	@Override
	public String getNameFile() {
		return "NotasAlumnos-"+getParametros().get("idMateria")+".xls";
	}

	@Override
	public List<TemplateXls<HashMap<String, String>>.Campo> getCamposGenerales() {
		ArrayList<Campo> campos = new ArrayList<Campo>();
		return campos;
	}

	@Override
	public List<TemplateXls<HashMap<String, String>>.Campo> getCamposTabla() {
		ArrayList<Campo> campos = new ArrayList<Campo>();
		
		campos.add(new Campo("anioModelo",  			"anioModelo",  		Tipo.STRING));
		campos.add(new Campo("color",  					"color",			Tipo.STRING));
		campos.add(new Campo("modelo", 					"modelo",  			Tipo.STRING));
		campos.add(new Campo("dominio",		  			"dominio",  		Tipo.STRING));

		return campos;
	}

	@Override
	public List<HashMap<String, String>> getDatosTabla(HashMap<String, String> parametros) {
		List<HashMap<String, String>> lista = new ArrayList<>();
		
		System.out.println("getDatosTabla()");
		System.out.println(getParametros());
		//Falta la implementación de buscar el entorno a utilizar.
		List<Vehiculo> listaVehiculos = servicio.getAll(null);
		for(Vehiculo vehiculo : listaVehiculos) {
			
			HashMap<String, String> vehiculoHash = new HashMap<String, String>();
			
			vehiculoHash.put("anioModelo", vehiculo.getAnioModelo()+"");
			vehiculoHash.put("color", vehiculo.getColor());
			vehiculoHash.put("modelo", vehiculo.getModelo());
			vehiculoHash.put("dominio", vehiculo.getDominio());
			
			lista.add(vehiculoHash);
		}
		
		System.out.println(lista);
		System.out.println("-------------------");
		return lista;
		

	}
	@Override
	public void configSheet(WritableSheet sheet) throws WriteException {
		sheet.setColumnView(1,  15);
		sheet.setColumnView(2,  40);
		sheet.setColumnView(3,  15);
		sheet.setColumnView(4,  15);
		sheet.setRowView((getInitFilaTabla()), 620);
	}
	@Override
	public HashMap<String, String> getDatosGenerales() {
		HashMap<String, String> datos = getParametros();
		System.out.println("getDatosGenerales()");
		System.out.println(datos);
		System.out.println("---------------------");
		return datos;
	}
	
	
	public static void main(String[]arg) throws RowsExceededException, WriteException, ServletException, IOException, ParseException {
		
		File file = new File("D:\\reportesTaller\\newfile.xls");
		FileOutputStream fop;
		try {
			fop = new FileOutputStream(file);
			new ReporteXls(null).export(fop, new HashMap<>());
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
