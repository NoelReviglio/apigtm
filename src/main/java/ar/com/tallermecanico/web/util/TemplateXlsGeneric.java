package ar.com.tallermecanico.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.springframework.beans.factory.xml.XmlBeanFactory;

import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.Number;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public abstract class TemplateXlsGeneric extends TemplateXls<HashMap<String, String>>{
	

	SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
	
	public void imprimirFila(HashMap<String, String> dato, int fila,WritableSheet sheet) throws WriteException, ParseException {
		int columna=getInitColumnaTabla();
		for(Campo campo: getCamposTabla()){
			columna = imprimirCelda(dato, fila, sheet, columna, campo);
		}
		columna=0;
	}

	
	protected int imprimirCelda(HashMap<String, String> dato, int fila,	WritableSheet sheet, int columna, Campo campo)
			throws NumberFormatException, WriteException, ParseException,
			RowsExceededException {
		WritableCell cell;
		WritableCellFormat cellFormat;
		if(campo.getTipoDato().equals(Tipo.NUMBER) && dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat(number);
			cell = new Number(columna++,fila, new Double( dato.get(campo.getKey())),cellFormat);
		}else if(campo.getTipoDato().equals(Tipo.INTEGER)&& dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat(integer);
			cell = new Number(columna++,fila, new Double( dato.get(campo.getKey())),cellFormat);
		}else if(campo.getTipoDato().equals(Tipo.IMPORTE)&& dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat(importe);
			cell = new Number(columna++,fila, new Double( dato.get(campo.getKey())),cellFormat);
		}else if(campo.getTipoDato().equals(Tipo.PORCENTAJE)&& dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat(porcentaje);
			cell = new Number(columna++,fila, new Double( dato.get(campo.getKey()))/100,cellFormat);
		}else if(campo.getTipoDato().equals(Tipo.DATE) && dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat(date);
			cell = new DateTime(columna++,fila,sdfDate.parse(dato.get(campo.getKey())),cellFormat);
		}else if(campo.getTipoDato().equals(Tipo.TIME) && dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat(time);
			cell = new Label(columna++,fila, dato.get(campo.getKey()),cellFormat);
		}else if(campo.getTipoDato().equals(Tipo.DATE_TIME) && dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat(date_time);
			cell = new Label(columna++,fila, dato.get(campo.getKey()),cellFormat);
		}else if(campo.getTipoDato().equals(Tipo.STRING)&& dato.get(campo.getKey())!=null){
			cellFormat = new WritableCellFormat();
			cell = new Label(columna++,fila, dato.get(campo.getKey()),cellFormat);
		}else{
			cellFormat = new WritableCellFormat();
			cell = new Label(columna++,fila, "",cellFormat);
		}
		setFilaStyle(cellFormat, campo, fila);

		sheet.addCell(cell);
		return columna;
	}

	protected void setFilaStyle(WritableCellFormat cellFormat, Campo campo, int fila) throws WriteException, ParseException  {
		cellFormat.setAlignment(campo.getAlignment());
		cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
	}

}
