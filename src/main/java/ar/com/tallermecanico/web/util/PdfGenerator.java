package ar.com.tallermecanico.web.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class PdfGenerator {

    public ByteArrayOutputStream getPDF() {

        // Creamos la instancia de memoria en la que se escribirá el archivo temporalmente
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            Document document = new Document(PageSize.A4);
            Calendar calendario = new GregorianCalendar();
            //Formato de letra para títulos
            Font Titulo = new Font();
            Titulo.setSize(20);
            //Formato para letra negrita
            Font negrita = new Font();
            negrita.setStyle(Font.BOLD);

            String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
            String mes = meses[calendario.get(Calendar.MONTH)];

            String total = "500";
            String estaPago = "Sí";

            //Partes del documento
           
            Chunk titulo = new Chunk("ORDEN DE SERVICIO");
            titulo.setUnderline(2f, -2f);
            titulo.setFont(Titulo);

            Chunk fecha = new Chunk(
                    "Villa María, " + calendario.get(Calendar.DATE) + " de " + mes + " de " + calendario.get(Calendar.YEAR));

            Paragraph trabajos = new Paragraph("Trabajos realizados: getDetalleOrdenServicio1, getDetalleOrdenServicio2, ... getDetalleORdenServicioN (USAR UN FOR PARA TRAER TODOS LOS DETALLES)");
            trabajos.setLeading(5.0f, 1.0f);

            Chunk montoTotal = new Chunk("Total= $" + total);

            Chunk firma = new Chunk("Firma: ");

            Chunk estado = new Chunk("PAGADO: " + estaPago);
            estado.setFont(negrita);

            //Contruimos la tabla donde se van a mostrar las partes
            PdfPTable tabla = new PdfPTable(1);

            PdfPCell celda0 = new PdfPCell(new Phrase(" "));
            PdfPCell celda1 = new PdfPCell(new Phrase(titulo));
            PdfPCell celda2 = new PdfPCell(new Phrase(fecha));
            PdfPCell celda3 = new PdfPCell(trabajos);
            PdfPCell celda4 = new PdfPCell(new Phrase(montoTotal));
            PdfPCell celda5 = new PdfPCell(new Phrase(firma));
            PdfPCell celda6 = new PdfPCell(new Phrase(estado));

            tabla.addCell(celda0);
            tabla.addCell(celda1);
            tabla.addCell(celda2);
            tabla.addCell(celda3);
            tabla.addCell(celda4);
            tabla.addCell(celda5);
            tabla.addCell(celda6);

            PdfPTable tabla2 = new PdfPTable(1);

            PdfPCell celda10 = new PdfPCell(new Phrase("Prueba10"));
            PdfPCell celda11 = new PdfPCell(new Phrase("Prueba11"));
            PdfPCell celda12 = new PdfPCell(new Phrase("Prueba12"));
            PdfPCell celda13 = new PdfPCell(new Phrase("Prueba13"));


            tabla.addCell(celda10);
            tabla.addCell(celda11);
            tabla.addCell(celda12);
            tabla.addCell(celda13);
      

            // Asignamos la variable ByteArrayOutputStream bos donde se escribirá el documento
            PdfWriter.getInstance(document, baos);
            document.open();
            document.add(tabla);
            document.add(tabla2);
            document.close();
            // Retornamos la variable al finalizar
            return baos;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
