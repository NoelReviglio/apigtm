package ar.com.tallermecanico.web.util;

public class Mensaje{
	private String estado;
	private String descripcion;
	
	public Mensaje(String estado, String descripcion) {
		super();
		this.estado = estado;
		this.descripcion = descripcion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}