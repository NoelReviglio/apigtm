package ar.com.tallermecanico.web.util;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.DateFormat;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public abstract class TemplateXls<H> {
	
	protected NumberFormat number ;
	protected NumberFormat integer;
	protected NumberFormat importe;
	protected NumberFormat porcentaje;
	protected DateFormat date;
	protected DateFormat time;
	protected DateFormat date_time;
	{
		number = new NumberFormat("###,###,##0.00");
		integer = new NumberFormat("###,###,##0");
		importe = new NumberFormat("$###,###,##0.00");
		porcentaje = new NumberFormat("###,###,##0.00%");
		TimeZone timeZone = new SimpleTimeZone(-3, "GMT");
		date = new DateFormat("dd/MM/yyyy");
		date.getDateFormat().setTimeZone(timeZone);
		time = new DateFormat("HH:mm");
		time.getDateFormat().setTimeZone(timeZone);
		date_time = new DateFormat("dd/MM/yyyy HH:mm");
		date_time.getDateFormat().setTimeZone(timeZone);
		
	}
	
	
	
	public abstract String getNameSheet();
	public abstract String getTitle();
	public abstract String getNameFile();
	public abstract List<Campo> getCamposGenerales();
	public abstract List<Campo> getCamposTabla();
	public abstract List<H> getDatosTabla(HashMap<String, String> parametros);
	public abstract void imprimirFila(H dato, int fila, WritableSheet sheet)throws WriteException, ParseException;
	public abstract void configSheet(WritableSheet sheet) throws WriteException;
	public abstract HashMap<String, String> getDatosGenerales();
	
	protected HashMap<String,String> parametros;
	protected List<H> datos ;
	private int initFilaTitulo=0;
	private int initColumnaTitulo=0;
	private int initFilaDatosGenerales=0;
	private int initColumnaDatosGenerales=0;
	private int initFilaTabla=0;
	private int initColumnaTabla=0;
	private WritableWorkbook workbook;
	public void export(HttpServletRequest request,
			HttpServletResponse response)  throws ServletException, IOException, RowsExceededException, WriteException, ParseException {
		parametros = getParametros(request);
		datos = getDatosTabla(parametros);
		WorkbookSettings settings = new WorkbookSettings();
		settings.setLocale(new Locale("es", "ar"));
		setWorkbook(Workbook.createWorkbook(response.getOutputStream(), settings));
		WritableSheet sheet = getWorkbook().createSheet(getNameSheet(), 0);
		init();
		initConfigSheet(sheet);
		configSheet(sheet);
		imprimirTitulo(sheet);
		imprimirDatosGenerales(sheet);
		imprimirDatosTabla(sheet);
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment;filename=\""+ getNameFile() + "\"");
		getWorkbook().write();
		getWorkbook().close();
	}
	
	public void export(OutputStream outputStream,HashMap<String, String> parametros )  throws ServletException, IOException, RowsExceededException, WriteException, ParseException {
		this.parametros = parametros ;
		datos = getDatosTabla(parametros);
		WorkbookSettings settings = new WorkbookSettings();
		settings.setLocale(new Locale("es", "ar"));
		setWorkbook(Workbook.createWorkbook(outputStream, settings));
		WritableSheet sheet = getWorkbook().createSheet(getNameSheet(), 0);
		init();
		initConfigSheet(sheet);
		configSheet(sheet);
		imprimirTitulo(sheet);
		imprimirDatosGenerales(sheet);
		imprimirDatosTabla(sheet);
//		response.setContentType("application/vnd.ms-excel");
//		response.setHeader("Content-Disposition",
//				"attachment;filename=\""+ getNameFile() + "\"");
		getWorkbook().write();
		getWorkbook().close();
	}
	
	/**
	 * Configuracion de posicion de TITULO, DATOS GENERALES y TABLA
	 */
	protected void init() {
		setInitColumnaTitulo(1);
		setInitFilaTitulo(1);
		
		setInitColumnaDatosGenerales(1);
		setInitFilaDatosGenerales(2);
		
		setInitColumnaTabla(1);
		setInitFilaTabla(getCamposGenerales().size()+3);
	}
	protected void initConfigSheet(WritableSheet sheet) throws RowsExceededException, WriteException {
		sheet.setColumnView(0, 2);
		sheet.setRowView(1, 450);
		sheet.setColumnView(1, 25);
		sheet.getSettings().setVerticalFreeze(getCamposGenerales().size()+ 4);
	}
	
	public HashMap<String, String> getParametros(HttpServletRequest request) {
		
		HashMap<String, String> parametros = new HashMap<String, String>();
		for(Object o : request.getParameterMap().keySet()){
			String valor = request.getParameter((String) o); 
			if(valor!=null && !valor.equals("null")){
				parametros.put((String) o,valor);
			}
		}
		return parametros;
	}

	
	public void imprimirTitulo(WritableSheet sheet) throws RowsExceededException, WriteException{
		WritableFont tituloFont = new WritableFont(WritableFont.ARIAL, 18, WritableFont.BOLD);
		WritableCellFormat tituloCellFormat = new WritableCellFormat();
		tituloCellFormat.setFont(tituloFont);
		tituloCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		sheet.mergeCells(getInitColumnaTitulo(),getInitFilaTitulo(), getCamposTabla().size(), getInitFilaTitulo() );
		Label tituloLabel = new Label(getInitColumnaTitulo(), getInitFilaTitulo(), getTitle(), tituloCellFormat);
		sheet.addCell(tituloLabel);		
		
	}
	public void imprimirDatosGenerales(WritableSheet sheet) throws WriteException {
		// 	Encabezados.
		List<Campo> filtros = getCamposGenerales();
		WritableFont       font;
		WritableCellFormat cellFormatLabels;
		font = new WritableFont(WritableFont.ARIAL,WritableFont.DEFAULT_POINT_SIZE,WritableFont.BOLD);
		font.setUnderlineStyle(UnderlineStyle.SINGLE);
		cellFormatLabels = new WritableCellFormat();
		cellFormatLabels.setFont(font);
		cellFormatLabels.setAlignment(Alignment.LEFT);
		
		WritableCellFormat cellFormatDatos;
		WritableFont       fontDatos;
		fontDatos = new WritableFont(WritableFont.ARIAL,11);
		cellFormatDatos = new WritableCellFormat();
		cellFormatDatos.setFont(fontDatos);
		cellFormatDatos.setAlignment(Alignment.LEFT);
		
		HashMap<String , String> datosGenerales = getDatosGenerales();
		
		NumberFormat nfImporte = importe;
		WritableCellFormat cellFormatImporte = new WritableCellFormat(nfImporte);
		cellFormatImporte.setFont(fontDatos);
		
		for(int i = 0; i < filtros.size(); ++i){
			sheet.addCell(new Label(getInitColumnaDatosGenerales(),i+getInitFilaDatosGenerales(), filtros.get(i).getName(), cellFormatLabels));
			
			if(datosGenerales.get(filtros.get(i).getKey())!=null){
				if(filtros.get(i).isHide()){
					CellView cellView = new CellView();
				    cellView.setHidden(true); 
					sheet.setRowView(i+getInitFilaDatosGenerales(), cellView);
				}
				if (filtros.get(i).getTipoDato().equals(Tipo.IMPORTE)){
					sheet.addCell(new Number(getInitColumnaDatosGenerales()+1,i+getInitFilaDatosGenerales(), Double.parseDouble(datosGenerales.get(filtros.get(i).getKey())), cellFormatImporte));
				}else{
					sheet.addCell(new Label(getInitColumnaDatosGenerales()+1,i+getInitFilaDatosGenerales(), datosGenerales.get(filtros.get(i).getKey()), cellFormatDatos));
				}
			}
			
			
		}
	}
	public void imprimirDatosTabla(WritableSheet sheet) throws WriteException, ParseException {
		int fila = getInitFilaTabla();
		List<Campo> encabezados = getCamposTabla();
		Label              encabezadoLabel;
		WritableFont       fontEncabezado;
		WritableCellFormat cellFormatEncabezado;
		
		fontEncabezado = new WritableFont(WritableFont.ARIAL,WritableFont.DEFAULT_POINT_SIZE,WritableFont.BOLD);
		cellFormatEncabezado = new WritableCellFormat();
		cellFormatEncabezado.setFont(fontEncabezado);
		cellFormatEncabezado.setBorder(Border.ALL, BorderLineStyle.THIN);
		cellFormatEncabezado.setBackground(Colour.GREY_25_PERCENT);
		cellFormatEncabezado.setAlignment(Alignment.CENTRE);
		cellFormatEncabezado.setVerticalAlignment(VerticalAlignment.CENTRE);
		cellFormatEncabezado.setWrap(true);
		
		for (int i = 0; i < encabezados.size(); ++i){
			encabezadoLabel = new Label(i+getInitColumnaTabla(), getInitFilaTabla(), encabezados.get(i).getName(),cellFormatEncabezado);
			sheet.addCell(encabezadoLabel);
		}
		if(datos!=null && datos.size()>0){
			for (H dato : datos) {
				fila++;
				imprimirFila(dato,fila,sheet);
			}
		}
	}
	public HashMap<String, String> getParametros() {
		return parametros;
	}

	public int getInitFilaTitulo() {
		return initFilaTitulo;
	}
	public void setInitFilaTitulo(int filaTitulo) {
		this.initFilaTitulo = filaTitulo;
	}

	public int getInitColumnaTitulo() {
		return initColumnaTitulo;
	}
	public void setInitColumnaTitulo(int columnaTitulo) {
		this.initColumnaTitulo = columnaTitulo;
	}

	public int getInitFilaDatosGenerales() {
		return initFilaDatosGenerales;
	}
	public void setInitFilaDatosGenerales(int filaDatosGenerales) {
		this.initFilaDatosGenerales = filaDatosGenerales;
	}

	public int getInitColumnaDatosGenerales() {
		return initColumnaDatosGenerales;
	}
	public void setInitColumnaDatosGenerales(int columnaDatosGenerales) {
		this.initColumnaDatosGenerales = columnaDatosGenerales;
	}

	public int getInitFilaTabla() {
		return initFilaTabla;
	}
	public void setInitFilaTabla(int filaTabla) {
		this.initFilaTabla = filaTabla;
	}

	public int getInitColumnaTabla() {
		return initColumnaTabla;
	}
	public void setInitColumnaTabla(int columnaTabla) {
		this.initColumnaTabla = columnaTabla;
	}

	public WritableWorkbook getWorkbook() {
		return workbook;
	}
	public void setWorkbook(WritableWorkbook workbook) {
		this.workbook = workbook;
	}

	public class Campo{
		
		private String name;
		private String key;
		private Tipo tipoDato;
		private Alignment alignment;
		private boolean hide;
		public Campo(String key,String name){
			this.setName(name);
			this.setKey(key);
		}
		public Campo(String key,String name, Tipo tipoDato){
			this(key,name);
			this.setTipoDato(tipoDato);
		}
		public Campo(String key,String name, Tipo tipoDato, boolean hide){
			this(key,name,tipoDato);
			setHide(hide);
		}
		public Campo(String key,String name, Tipo tipoDato,Alignment alignment){
			this(key,name,tipoDato);
			this.alignment = alignment;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public Tipo getTipoDato() {
			return tipoDato;
		}
		public void setTipoDato(Tipo tipoDato) {
			this.tipoDato = tipoDato;
			if(Tipo.NUMBER.equals(tipoDato)){
				setAlignment(Alignment.RIGHT);
			}else if(Tipo.INTEGER.equals(tipoDato)){
				setAlignment(Alignment.RIGHT);
			}else if(Tipo.IMPORTE.equals(tipoDato)){
				setAlignment(Alignment.RIGHT);
			}else if(Tipo.DATE.equals(tipoDato)){
				setAlignment(Alignment.CENTRE);
			}else if(Tipo.STRING.equals(tipoDato)){
				setAlignment(Alignment.LEFT);
			}else if(Tipo.PORCENTAJE.equals(tipoDato)){
				setAlignment(Alignment.RIGHT);
			}else{
				setAlignment(Alignment.CENTRE);
			}
		}
		public Alignment getAlignment(){
			return alignment;
		}
		public void setAlignment(Alignment alignment) {
			this.alignment = alignment;
		}
		public boolean isHide() {
			return hide;
		}
		/**
		 * 
		 * @param hide
		 * oculta la fila del excel
		 */
		public void setHide(boolean hide) {
			
			this.hide = hide;
		}
	}
	public enum Tipo{
		STRING,NUMBER,INTEGER,DATE,IMPORTE,PORCENTAJE,TIME,DATE_TIME;
	}
}
