package ar.com.tallermecanico.web.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ar.com.tallermecanico.web.JWTAuthorizationFilter;
import ar.com.tallermecanico.web.modelo.Entorno;
import ar.com.tallermecanico.web.repositorio.EntornoRepositorio;
import io.jsonwebtoken.Claims;
@Component
public class ValidarEntorno {
	@Value("${config.auth}")
	private String auth;
	@Autowired
	private EntornoRepositorio repositorio;
	
	
	public Entorno obtenerEntorno(HttpServletRequest request) {	
		if("SI".equals(auth)) {
			Claims claims = JWTAuthorizationFilter.validateToken(request);
			return repositorio.getOne((Integer) claims.get("entorno"));
			
		} else {
			return repositorio.getOne(0);
		}

	}
	
	public String obtenerUsuario(HttpServletRequest request) {	
		if("SI".equals(auth)) {
			Claims claims = JWTAuthorizationFilter.validateToken(request);
			return (String) claims.get("usuario");
			
		} else {
			return System.getProperty("user.name");
		}

	}
}
