package ar.com.tallermecanico.web.util;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class EJEMPLOCenterColumnVertically {
	//https://kb.itextpdf.com/home/it5kb/examples/positioning-different-text-snippets-on-a-page
    public static final String DEST = "D:\\reportesTaller\\centerColumn.pdf";
    private static final String iTextExampleImage = "D:/reportesTaller/image.png";
    private static final Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 26, Font.BOLDITALIC);
    private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
    
    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new EJEMPLOCenterColumnVertically().createPdf(DEST);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        // step 3
        document.open();
        // step 4
        // show the area for the column as a rectangle with red borders
        float llx = 50;  float lly = 50;
        float urx = 50; float ury = 50;
        Rectangle rect = new Rectangle(llx, lly, urx, ury);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(0.5f);
        rect.setBorderColor(BaseColor.RED);
        PdfContentByte cb = writer.getDirectContent();
        cb.rectangle(rect);
        //
          // First page
            // Primera página 
            Chunk chunk = new Chunk("This is the title", chapterFont);
            chunk.setBackground(BaseColor.GRAY);
        Chapter chapter = new Chapter(new Paragraph(chunk), 1);
            chapter.setNumberDepth(0);
            chapter.add(new Paragraph("This is the paragraph", paragraphFont));
         // We add an image (Añadimos una imagen)
         Image image;
         try {
             image = Image.getInstance(iTextExampleImage);  
             image.setAbsolutePosition(2, 150);
             chapter.add(image);
         } catch (BadElementException ex) {
             System.out.println("Image BadElementException" +  ex);
         } catch (IOException ex) {
             System.out.println("Image IOException " +  ex);
         }
         document.add(chapter);
        // this is the paragraph we want to center vertically:       
        Paragraph p = new Paragraph("Noel");
        p.setLeading(12f);
        // We add the column in simulation mode:
        float y = drawColumnText(cb, rect, p, true);
        // We calculate a new rectangle and add the column for real
        rect = new Rectangle(llx, lly, urx, ury - ((y - lly) / 2));
        drawColumnText(cb, rect, p, false);
        document.add(p);
        // step 5
        document.close();
    }

    /**
     * Draws a Paragraph inside a given column and returns the Y value at the end of the text.
     * @param  canvas    the canvas to which we'll add the Paragraph
     * @param  rect      the dimensions of the column
     * @param  p         the Paragraph we want to add
     * @param  simulate  do we add the paragraph for real?
     * @return the Y coordinate of the end of the text
     * @throws com.itextpdf.text.DocumentException
     */
    public float drawColumnText(PdfContentByte canvas, Rectangle rect, Paragraph p, boolean simulate) throws DocumentException {
        ColumnText ct = new ColumnText(canvas);
        ct.setSimpleColumn(rect);
        ct.addElement(p);
        ct.go(simulate);
        return ct.getYLine();
    }
}
