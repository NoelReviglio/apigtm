package ar.com.tallermecanico.web.util;

import java.util.ArrayList;
import java.util.List;

public class Validaciones extends Exception {

    public List<Mensaje> mensajes = new ArrayList<Mensaje>();

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    } 
    
    public boolean isValid() {
        return mensajes.size()==0;
    }

    public String toString(){
        return ("Se ha producido la excepción: " + "\n" + this.getClass().getName() + "\n" + "Con el siguiente mensaje: " + this.getMessage() + "\n");
    }
}

