package ar.com.tallermecanico.web.util;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BadPdfFormatException;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import ar.com.tallermecanico.web.modelo.Entorno;

// Agregar Header y Footer para todas las páginas
    class HeaderYFooter extends PdfPageEventHelper {

        private static final Font font4 = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
        private static final Font smallfont = new Font(Font.FontFamily.HELVETICA, 5, Font.ITALIC);
        private static final Font redFont = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.RED);

        Entorno entorno;
        String usuario;
        public HeaderYFooter(Entorno entorno, String usuario) {
			this.entorno = entorno;
			this.usuario = usuario;
		}

		public void onEndPage(PdfWriter writer, Document document) {
			
			
	        try {
	        	PdfContentByte content = writer.getDirectContent();
	        	Image image = Image.getInstance(new URL(entorno.getUrl()));
	        	image.scaleToFit(50,50);
	        	image.setAbsolutePosition(document.getPageSize().getWidth()-75, document.getPageSize().getHeight()-50);
	        	content.addImage(image);
			} catch (BadPdfFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadElementException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
            PdfContentByte cb = writer.getDirectContent();
            Phrase header1 = new Phrase(entorno.getNombre(), redFont);
            Phrase footer1 = new Phrase("DIRECCIÓN: "+entorno.getDireccion()+" - TELÉFONOS: "+entorno.getTelefono(), font4);
            Phrase vacio = new Phrase(" ");
            Phrase footer2 = new Phrase("Emitido por: " + usuario + " - " + obtenerFechaYHoraActual(), smallfont);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header1, (document.right() - document.left()) / 2 + document.leftMargin(), document.top() + 12, 0);
                //ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header2, (document.right() - document.left()) / 2 + document.leftMargin(), document.top() + 2, 0);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer1, (document.right() - document.left()) / 2 + document.leftMargin(), document.bottom() - 2, 0);
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer2, (document.right() - document.left()) / 2 + document.leftMargin(), document.bottom() - 12, 0);
        }

        public static String obtenerFechaYHoraActual() {
            String formato = "dd/MM/yyyy HH:mm:ss";
            DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formato);
            LocalDateTime ahora = LocalDateTime.now();
            return formateador.format(ahora);
        }



    }