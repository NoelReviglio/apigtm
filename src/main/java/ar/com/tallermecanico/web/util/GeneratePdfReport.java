package ar.com.tallermecanico.web.util;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.tallermecanico.web.modelo.DetalleOrdenDeServicio;
import ar.com.tallermecanico.web.modelo.Entorno;
import ar.com.tallermecanico.web.modelo.OrdenDeServicio;
import ar.com.tallermecanico.web.modelo.Vehiculo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class GeneratePdfReport {

    private static final Logger logger = LoggerFactory.getLogger(GeneratePdfReport.class);
    
    private static final Font titulo1Font = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
    private static final Font titulo2Font = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
    private static final Font titulo3Font = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
    private static final Font font4 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
    private static final Font blueFont = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLUE);
    private static final Font redFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.RED);
    private static final String logo = "D:/imagenesTallerMec/logo.png";
    private static final Font smallfont = new Font(Font.FontFamily.UNDEFINED, 5, Font.ITALIC);
    private static final Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

    public static ByteArrayInputStream vehiculosPdf(java.util.List<Vehiculo> vehiculos,Entorno entorno,String usuario) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // Inicialización del PDF
        String titulo = "Listado de Vehículos";
        try {
            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.getInstance(document, baos);
            document.open();
            HeaderYFooter event = new HeaderYFooter(entorno,usuario);
            writer.setPageEvent(event);
            addEncabezado(document);
            addMetaData(document, titulo);
            addTitlePage(document, titulo);
            addContent(document, vehiculos);
            document.close();
            System.out.println("El PDF se ha generado correctamente!");
        } catch (DocumentException documentException) {
            System.out.println("Se ha producido un error al generar el documento: " + documentException);
            logger.error("Ocurrió un error", documentException);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }

    public static ByteArrayInputStream ordenDeServicioPdf(OrdenDeServicio os,Entorno entorno,String usuario) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // Inicialización del PDF
        String titulo = "Orden de Servicio N°: " + os.getCodigo();
        try {
            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.getInstance(document, baos);
            document.open();
            HeaderYFooter event = new HeaderYFooter(entorno,usuario);
            writer.setPageEvent(event);
            addEncabezado(document);
            addMetaData(document, titulo);
            addTitlePage(document, titulo);
            addContent(document, os);
            document.close();
            System.out.println("El PDF se ha generado correctamente!");
        } catch (DocumentException documentException) {
            System.out.println("Se ha producido un error al generar el documento: " + documentException);
            logger.error("Ocurrió un error", documentException);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }

    // PARTES DEL REPORTE

    // Agregar MetaData
    private static void addMetaData(Document document, String titulo) {
        document.addTitle(titulo);
        document.addSubject("Usando iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("SerLiNo");
        document.addCreator("SerLiNo");
    }

    // Agregar Encabezado
    private static void addEncabezado(Document document) throws DocumentException {
        // We add an image
        Image imagen;
        try {
            imagen = Image.getInstance(logo);
            //imagen.setAbsolutePosition(2, 15);
            PdfPTable table = new PdfPTable(1);
            table.setWidthPercentage(30);            
            table.addCell(imagen);
            document.add(table); 
        } catch (BadElementException ex) {
            System.out.println("Image BadElementException" + ex);
        } catch (IOException ex) {
            System.out.println("Image IOException " + ex);
        }
        //Subtítulo para logo
        /* Paragraph tm = new Paragraph("Taller Mecánico RULEMAN", blueFont);
        tm.setLeading(1, 1);
        tm.setAlignment(Element.ALIGN_CENTER);
        document.add(tm); */
    }

    //Agregar Título
    private static void addTitlePage(Document document, String titulo) throws DocumentException {
        Paragraph p = new Paragraph();
        p.add(new Paragraph(titulo.toUpperCase(), titulo1Font));
        addEmptyLine(p, 1);
        p.setLeading(1, 1);
        p.setAlignment(Element.ALIGN_CENTER);
        p.setIndentationLeft(160);
        document.add(p);
        // Start a new page - Luego de cargar todo lo correspondiente al título, genera una nueva hoja
        //document.newPage();
    }

    //SOBREESCRIBIR METODO ADDCONTENT SEGUN PARAMETROS PARA CADA REPORTE

    //Agregar contenido: Reporte Vehiculos
    private static void addContent(Document document, java.util.List<Vehiculo> vehiculos) throws DocumentException {
        Paragraph p = new Paragraph();
        p.add(new Paragraph("Listado de vehículos por Dominio:", titulo2Font));
        // add a list
        createList(p, vehiculos);
        addEmptyLine(p, 1);

        Paragraph p2 = new Paragraph();
        p2.add(new Paragraph("Tabla de Vehículos: ", titulo2Font));
        addEmptyLine(p2, 1);
        // add a table
        createTable(p2, vehiculos);

        // now add all this to the document
        document.add(p);
        document.newPage();
        document.add(p2);
    }

    
    //Agregar contenido: Reporte Orden de Servicio
    private static void addContent(Document document, OrdenDeServicio os) throws DocumentException {
        //Calcular nivel de combustible
        String nivel;
        switch(os.getNivelCombustible().toString()) {
            case "0.00":
                nivel = "Vacío";
            break;

            case "0.25":
                nivel = "1/4";
            break;

            case "0.50":
                nivel = "1/2";
            break;

            case "0.75":
                nivel = "3/4";
            break;

            case "1.00":
                nivel = "Lleno";
            break;

            default:
                nivel = "No especificado";
        }

        // add a table
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.setWidths(new int[]{4, 2});

        PdfPCell celda;
        
        //FILA 1: Número y Fecha
        celda = new PdfPCell(new Phrase("ORDEN N°: " + os.getCodigo(), titulo3Font));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);

        celda = new PdfPCell(new Phrase("FECHA: " + formatearFecha(os.getFecha()), headFont));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        //FILA 2: Vacía
        celda = new PdfPCell(new Phrase(" "));
        celda.setBorder(0);
        table.addCell(celda);
        table.addCell(celda);
        //FILA 3: Titular y Teléfono
        if (os.getRazonSocialTitular() == null) {
            celda = new PdfPCell(new Phrase("Titular: " + os.getApellidoTitular().toUpperCase() + ", " + os.getNombreTitular().toUpperCase()));
        } else {
            celda = new PdfPCell(new Phrase("Titular: " + os.getRazonSocialTitular().toUpperCase()));    
        }
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);

        celda = new PdfPCell(new Phrase("Teléfono: " + os.getTelefonoTitular()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        //FILA 4: Chofer y Teléfono
        celda = new PdfPCell(new Phrase("Chofer: " + os.getApellidoChofer().toUpperCase() + ", " + os.getNombreChofer().toUpperCase()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        
        celda = new PdfPCell(new Phrase("Teléfono: " + os.getTelefonoChofer()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        //FILA 5: Dominio y Marca
        celda = new PdfPCell(new Phrase("Dominio: " + os.getVehiculo().getDominio()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);

        celda = new PdfPCell(new Phrase("Marca: " + os.getVehiculo().getMarca().getNombre().toUpperCase()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        //FILA 6: Modelo y Año
        celda = new PdfPCell(new Phrase("Modelo: " + os.getVehiculo().getModelo().toUpperCase()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);

        celda = new PdfPCell(new Phrase("Año: " + os.getVehiculo().getAnioModelo()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        //FILA 7: Chasis y Color
        celda = new PdfPCell(new Phrase("Nº Chasis: " + os.getVehiculo().getChasis().toUpperCase()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);

        celda = new PdfPCell(new Phrase("Color: " + os.getVehiculo().getColor().toUpperCase()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        //FILA 8: Kms y Nivel Combustible
        celda = new PdfPCell(new Phrase("Kilometraje actual: " + os.getKilometrajeActual()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);

        celda = new PdfPCell(new Phrase("Nivel de combustible: " + nivel.toUpperCase()));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(0);
        table.addCell(celda);
        //FIN TABLA

        Paragraph p2 = new Paragraph();
        if (os.getFechaRecepcion()!=null && os.getFechaRecepcion()!=null) {
            p2.add(new Paragraph("RECEPCIÓN: " + formatearFecha(os.getFechaRecepcion()) + " - " + os.getHoraRecepcion() + "hs."));
            addEmptyLine(p2, 1);
        } else {
            if (os.getFechaRecepcion()!=null) {
                p2.add(new Paragraph("RECEPCIÓN: " + formatearFecha(os.getFechaRecepcion()) + " - --:--hs."));
                addEmptyLine(p2, 1);    
            } else {
                p2.add(new Paragraph("RECEPCIÓN: --/--/---- - --:--hs."));
                addEmptyLine(p2, 1);
            }
        }

        Paragraph p3 = new Paragraph();
        p3.add(new Paragraph("MOTIVO DE LA VISITA: " + os.getMotivoVisita()));
        addEmptyLine(p3, 1);

        Paragraph p4 = new Paragraph();
        p4.add(new Paragraph("Detalle de trabajos: ", titulo3Font));
        addEmptyLine(p4, 1);

        //TABLA DETALLE DE SERVICIOS
        PdfPTable tablaDetalles = new PdfPTable(2);

        tablaDetalles.setWidthPercentage(90);
        tablaDetalles.setWidths(new int[]{3, 1});

        PdfPCell cd; 
        cd = new PdfPCell(new Phrase("Detalle", headFont));
        cd.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaDetalles.addCell(cd);

        cd = new PdfPCell(new Phrase("Subtotal", headFont));
        cd.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaDetalles.addCell(cd);

        tablaDetalles.setHeaderRows(1);

        for (DetalleOrdenDeServicio dos : os.getDetalle()) {
            PdfPCell cell;

            cell = new PdfPCell(new Phrase(dos.getDescripcion()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDetalles.addCell(cell);

            cell = new PdfPCell(new Phrase(dos.getSubtotal().toString()));
            cell.setPaddingLeft(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalles.addCell(cell);
        }

        //PENÚLTIMA FILA PARA TOTAL
        PdfPCell cellTotal; 
        cellTotal = new PdfPCell(new Phrase("TOTAL $: ", titulo3Font));
        cellTotal.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellTotal.setBorder(0);
        tablaDetalles.addCell(cellTotal);

        cellTotal = new PdfPCell(new Phrase(os.getTotal().toString(), titulo3Font));
        cellTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellTotal.setBorder(0);
        tablaDetalles.addCell(cellTotal);
        //ÚLTIMA FILA VACÍA
        cellTotal = new PdfPCell(new Phrase(" "));
        cellTotal.setBorder(0);
        tablaDetalles.addCell(cellTotal);

        cellTotal = new PdfPCell(new Phrase(" "));
        cellTotal.setBorder(0);
        tablaDetalles.addCell(cellTotal);

        //FIN TABLA DETALLES
        
        Paragraph p5 = new Paragraph();
        if (os.isAbonada()==false) {
            p5.add(new Paragraph("Condición: NO ABONADA", redFont));
            p5.setAlignment(Element.ALIGN_LEFT);
            addEmptyLine(p5, 1);
        } else {
            p5.add(new Paragraph("Condición: ABONADA", titulo2Font));
            p5.setAlignment(Element.ALIGN_LEFT);
            addEmptyLine(p5, 1);
        }
        
        Paragraph p6 = new Paragraph();
        p5.add(new Paragraph("Firma y Aclaración CLIENTE: "));
        p5.setAlignment(Element.ALIGN_LEFT);
        addEmptyLine(p6, 1);
        
        // now add all this to the document
        document.add(table);
        document.add(p2);
        document.add(p3);
        document.add(p4);
        document.add(tablaDetalles);
        document.add(p5);
        document.add(p6);
    }

    //Agregamos una tabla
    private static void createTable(Paragraph p, java.util.List<Vehiculo> vehiculos) throws BadElementException {
        //Tabla de 3 columnas
        PdfPTable table = new PdfPTable(3);

        table.setWidthPercentage(100);
        //table.setWidths(new int[]{1, 3, 3});

        // t.setBorderColor(BaseColor.GRAY);
        // t.setPadding(4);
        // t.setSpacing(4);
        // t.setBorderWidth(1);

        PdfPCell c1; 
        c1 = new PdfPCell(new Phrase("Dominio", headFont));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Marca", headFont));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Propietario", headFont));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        table.setHeaderRows(1);

        for (Vehiculo v : vehiculos) {

            PdfPCell cell;

            cell = new PdfPCell(new Phrase(v.getDominio()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(v.getMarca().getNombre()));
            cell.setPaddingLeft(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            if (v.getRazonSocialPropietario() == null) {
                cell = new PdfPCell(new Phrase(v.getApellidoPropietario() + ", " + v.getNombrePropietario()));
            } else {
                cell = new PdfPCell(new Phrase(v.getRazonSocialPropietario()));   
            }
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingRight(5);
            table.addCell(cell);
        }

        p.add(table);

    }

    //Agregamos una lista
    private static void createList(Paragraph p, java.util.List<Vehiculo> vehiculos) {
        List list = new List(true, false, 10);
        for (Vehiculo v : vehiculos) {
            list.add(new ListItem("  " + v.getDominio()));
        }
        p.add(list);
    }

    //Agregar líneas vacías
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    public static String obtenerFechaYHoraActual() {
		String formato = "dd/MM/yyyy HH:mm:ss";
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formato);
		LocalDateTime ahora = LocalDateTime.now();
		return formateador.format(ahora);
    }
    
    public static String formatearFecha(LocalDate fecha) {
		String formato = "dd/MM/yyyy";
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formato);
		return formateador.format(fecha);
	}


}
