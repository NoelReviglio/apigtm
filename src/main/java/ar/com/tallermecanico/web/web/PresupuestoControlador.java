package ar.com.tallermecanico.web.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.tallermecanico.web.logica.PresupuestoServicio;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/presupuesto")
public class PresupuestoControlador {
	
	@Autowired
	private PresupuestoServicio servicio;
}
