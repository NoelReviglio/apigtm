package ar.com.tallermecanico.web.web;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ar.com.tallermecanico.web.logica.VehiculoServicio;
import ar.com.tallermecanico.web.util.ReporteXls;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.springframework.http.MediaType;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/excel")
public class ExportToExcel{
	
	@Autowired
	private VehiculoServicio servicio;
	@GetMapping
	public String getArchivo(final HttpServletRequest request, final HttpServletResponse response) {
//	 public ResponseEntity<StreamingResponseBody> getArchivo(final HttpServletRequest request, final HttpServletResponse response){
		 
//		 response.setContentType("application/zip");
//	        response.setHeader(
//	                "Content-Disposition",
//	                "attachment;filename=sample.zip");

//	        StreamingResponseBody stream = out -> {

//	            final String home = System.getProperty("user.home");
//	            final File directory = new File(home + File.separator + "Documents" + File.separator + "sample");
//	            final ZipOutputStream zipOut = new ZipOutputStream(response.getOutputStream());
	        
//	            if(directory.exists() && directory.isDirectory()) {
//	                try {
//	                    for (final File file : directory.listFiles()) {
//	                        final InputStream inputStream=new FileInputStream(file);
//	                        final ZipEntry zipEntry=new ZipEntry(file.getName());
//	                        zipOut.putNextEntry(zipEntry);
//	                        byte[] bytes=new byte[1024];
//	                        int length;
//	                        while ((length=inputStream.read(bytes)) >= 0) {
//	                            zipOut.write(bytes, 0, length);
//	                        }
//	                        inputStream.close();
//	                    }
//	                    zipOut.close();
//	                } catch (final IOException e) {
//	                    logger.error("Exception while reading and streaming data {} ", e);
//	                }
//	            }
//	        };
	        
	        
	        
	        try {
	        	new ReporteXls(servicio).export(request, response);
//				return new ResponseEntity(response.getOutputStream(), HttpStatus.OK)caca;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return "Archivo generado";
	 }
	 
	 
}
