package ar.com.tallermecanico.web.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.tallermecanico.web.logica.MarcaServicio;
import ar.com.tallermecanico.web.modelo.Marca;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/marca")
public class MarcaControlador {

    @Autowired
    private MarcaServicio servicio;
    
    // CALL CONSULTA

    @GetMapping
    public List<Marca> getAllMarca() {
        return servicio.getAllMarca();
    }
}