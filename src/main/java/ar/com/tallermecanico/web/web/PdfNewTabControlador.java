package ar.com.tallermecanico.web.web;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.tallermecanico.web.logica.OrdenDeServicioServicio;
import ar.com.tallermecanico.web.logica.VehiculoServicio;
import ar.com.tallermecanico.web.modelo.Entorno;
import ar.com.tallermecanico.web.modelo.OrdenDeServicio;
import ar.com.tallermecanico.web.modelo.Vehiculo;
import ar.com.tallermecanico.web.util.GeneratePdfReport;
import ar.com.tallermecanico.web.util.ValidarEntorno;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/reportes")
public class PdfNewTabControlador {

    @Autowired
    private VehiculoServicio servicioV;
    @Autowired
    private OrdenDeServicioServicio servicioOS;
    @Autowired
    private ValidarEntorno validarEntorno;

    //Reporte Vehiculos
    @RequestMapping(value = "/vehiculos", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> reporteVehiculos(HttpServletRequest request,HttpServletResponse response) {
    	Entorno entorno = validarEntorno.obtenerEntorno(request);
        List<Vehiculo> vehiculos = servicioV.getAll(entorno);
        String usuario = validarEntorno.obtenerUsuario(request);
        ByteArrayInputStream bais = GeneratePdfReport.vehiculosPdf(vehiculos,entorno,usuario);
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=vehiculosPdf.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bais));
    }

    //Reporte Orden de Servicio
    @RequestMapping(value = "/ordendeservicio/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> reporteOrdenDeServicio(@PathVariable(name = "codigo") Long codigo,HttpServletRequest request,HttpServletResponse response) {
    	Entorno entorno = validarEntorno.obtenerEntorno(request);
    	String usuario = validarEntorno.obtenerUsuario(request);
        OrdenDeServicio os = servicioOS.consultarOrdenDeServicio(codigo, entorno);
        ByteArrayInputStream bais = GeneratePdfReport.ordenDeServicioPdf(os,entorno,usuario);
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=ordenDeServicio.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bais));
    }
}
