package ar.com.tallermecanico.web.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.tallermecanico.web.logica.OrdenDeServicioServicio;
import ar.com.tallermecanico.web.modelo.OrdenDeServicio;
import ar.com.tallermecanico.web.modelo.Vehiculo;
import ar.com.tallermecanico.web.util.Respuesta;
import ar.com.tallermecanico.web.util.ValidarEntorno;

@RestController
@RequestMapping("/ordendeservicio")
@CrossOrigin(origins = "*")
public class OrdenDeServicioControlador {

    @Autowired
    private OrdenDeServicioServicio servicio;
    @Autowired
    private ValidarEntorno validarEntorno;
    // CALL ALTA
    @PostMapping
    public Respuesta registrarOrdenDeServicio(@RequestBody OrdenDeServicio os, HttpServletRequest request,HttpServletResponse response) {
        return servicio.registrarOrdenDeServicio(os, validarEntorno.obtenerEntorno(request));
    }

    // CALL BAJA
    @RequestMapping(value = "/{codigo}" , method = RequestMethod.DELETE)
    public Respuesta anularOrdenDeServicio(@PathVariable(name = "codigo") Long codigo, HttpServletRequest request,HttpServletResponse response) {
        return servicio.anularOrdenDeServicio(codigo, validarEntorno.obtenerEntorno(request));
    }

    // CALL MODIFICACION
    @RequestMapping(value = "/{codigo}" , method = RequestMethod.PUT)
    public Respuesta modificarOrdenDeServicio(@RequestBody OrdenDeServicio os, @PathVariable(name = "codigo") Long codigo, HttpServletRequest request,HttpServletResponse response) {
        return servicio.modificarOrdenDeServicio(os, validarEntorno.obtenerEntorno(request));
    }

    // CALL CONSULTA
    @GetMapping(value = "/{codigo}")
    public Page<OrdenDeServicio> consultarOrdenDeServicio(@PathVariable(name = "codigo") Long codigo, HttpServletRequest request,HttpServletResponse response, Pageable pagina) {
        return servicio.consultarOrdenDeServicio(codigo, validarEntorno.obtenerEntorno(request), pagina);
    }
    
    @GetMapping
    public Page<OrdenDeServicio> getAllOrdenDeServicio(HttpServletRequest request,HttpServletResponse response, Pageable pagina) {
        return servicio.getAllOrdenDeServicio(validarEntorno.obtenerEntorno(request), pagina);
    }

    @GetMapping(value = "/listar/{dominio}")
    public Page<OrdenDeServicio> obtenerOSporVehiculo(@PathVariable(name = "dominio") String dominio, HttpServletRequest request,HttpServletResponse response, Pageable pagina) {
        return servicio.obtenerOSporVehiculo(dominio, validarEntorno.obtenerEntorno(request), pagina);
    }
        
    // CALL ESTADOS
    @RequestMapping(value = "/abonada" , method = RequestMethod.PUT)
    public Respuesta abonarOrdenDeServicio(@RequestBody OrdenDeServicio os, HttpServletRequest request,HttpServletResponse response) {
        return servicio.abonarOrdenDeServicio(os, validarEntorno.obtenerEntorno(request));
    }
    
    @RequestMapping(value = "/cerrada" , method = RequestMethod.PUT)
    public Respuesta cerrarOrdenDeServicio(@RequestBody OrdenDeServicio os, HttpServletRequest request,HttpServletResponse response) {
        return servicio.cerrarOrdenDeServicio(os, validarEntorno.obtenerEntorno(request));
    }
}