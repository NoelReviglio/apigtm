package ar.com.tallermecanico.web.web;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.tallermecanico.web.util.ITextPrueba;
import ar.com.tallermecanico.web.util.PdfGenerator;

import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.DocumentException;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
@CrossOrigin(origins = "*")
@Controller
public class PdfDownloadControlador {

    @GetMapping("/pdf1")
    public void downloadFile(HttpServletResponse response) throws IOException {
        PdfGenerator generator = new PdfGenerator();
        byte[] pdfReport = generator.getPDF().toByteArray();

        String mimeType =  "application/pdf";
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", "reporte.pdf"));

        response.setContentLength(pdfReport.length);

        ByteArrayInputStream bais = new ByteArrayInputStream(pdfReport);
        FileCopyUtils.copy(bais, response.getOutputStream());
    }

    
    public static final String DEST = "D://reportesTaller/PruebaIText.pdf";

    @GetMapping("/pdf2")
    public void nosequehace() throws DocumentException, IOException {
        new ITextPrueba().createPdf(DEST);
    }

}
