package ar.com.tallermecanico.web.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.tallermecanico.web.logica.VehiculoServicio;
import ar.com.tallermecanico.web.modelo.Vehiculo;
import ar.com.tallermecanico.web.util.Respuesta;
import ar.com.tallermecanico.web.util.ValidarEntorno;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/vehiculo")
public class VehiculoControlador {

    @Autowired
    private VehiculoServicio servicio;

    @Autowired
    private ValidarEntorno validarEntorno;
    
    // CALL ALTA
    @PostMapping
    public Respuesta registrarVehiculo(@RequestBody Vehiculo v, HttpServletRequest request,HttpServletResponse response) { 
    	return servicio.registrarVehiculo(v, validarEntorno.obtenerEntorno(request));
    }

    // CALL BAJA
    @RequestMapping(value = "/{id}" , method = RequestMethod.DELETE)
    public Respuesta eliminarVehiculo(@PathVariable(name = "id") Long id) {
        return servicio.eliminarVehiculo(id);
    }

    // CALL MODIFICACION
    @RequestMapping(value = "/{id}" , method = RequestMethod.PUT)
    public Respuesta modificarVehiculo(@RequestBody Vehiculo v, @PathVariable(name = "id") Long id, HttpServletRequest request) {
        return servicio.modificarVehiculo(v,validarEntorno.obtenerEntorno(request));
    }

    // CALL CONSULTA
    @GetMapping(value = "/{dominioExacto}")
    public Page<Vehiculo> consultarVehiculo(@PathVariable(name = "dominioExacto") String dominioExacto, HttpServletRequest request,HttpServletResponse response, Pageable pagina) {
        return servicio.consultarVehiculo(dominioExacto, validarEntorno.obtenerEntorno(request), pagina);
    }

    @GetMapping
    public Page<Vehiculo> getAllVehiculo(Pageable pagina, HttpServletRequest request,HttpServletResponse response) {
        return servicio.getAllVehiculo(validarEntorno.obtenerEntorno(request),pagina);
    }

    @GetMapping(params = {"dominio"})
    public Page<Vehiculo> getAllVehiculoByDominio(String dominio, HttpServletRequest request,HttpServletResponse response, Pageable pagina) {
        return servicio.getAllVehiculoByDominio(dominio, validarEntorno.obtenerEntorno(request), pagina);
    }

}
